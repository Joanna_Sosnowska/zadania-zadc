Praca domowa #C
===============

Pełne zadanie laboratoryjne (na 2 pkt.) wdrożyć na innej chmurze niż Heroku.
Opisać swoje spostrzeżenia i porównać możliwości i obsługę wybranej chmury z Heroku
na co najmniej 200 liter.

Propozycje chmur PaaS
---------------------
* `Google App Engine <https://developers.google.com/appengine>`__
* `OpenShift <https://www.openshift.com>`__
* `Cloud66 <https://www.cloud66.com>`__
* `Microsoft Azure <http://azure.microsoft.com>`__
